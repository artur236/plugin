import { DOMMessage, DOMMessageResponse } from '../types';

const messagesFromReactAppListener = (msg: DOMMessage, sender: chrome.runtime.MessageSender, sendResponse: (response: DOMMessageResponse) => void) => {
  let response: DOMMessageResponse;

  if (msg.type === 'GET_DOM') {
    if (msg.url.includes('https://www.work.ua/')) {
      const card = document.getElementsByClassName('card-indent')[0];
      const cardHTML = card && card.innerHTML && card.innerHTML.replace(/\n/g, '').replace(/\s{3,}/g, '');
      const experienceString = cardHTML && cardHTML.match(/<h2>Досвід роботи<\/h2>.*<h2>Освіта<\/h2>/) && cardHTML.match(/<h2>Досвід роботи<\/h2>.*<h2>Освіта<\/h2>/)[0];
      const doc = new DOMParser().parseFromString(experienceString, 'text/html');
      const headers = doc && doc.querySelectorAll('h2');
      const muted = doc && doc.querySelectorAll('p.text-muted');
      headers && headers.length > 0 && headers.forEach(header => header.remove());
      muted && muted.length && muted.forEach(m => m.remove());
      doc && doc.querySelector('hr') && doc.querySelector('hr').remove();
      const experience = doc.body.innerHTML.replace(/<[^\/>]*>/g, '').replace(/<[^>]*>/g, ';').replace(/&nbsp;/g, ' ');
      const socialLinks = (document.querySelectorAll('.social-network-link') as unknown as HTMLAnchorElement[]);
      const linkedInLink = Array.from(socialLinks).filter((link: any) => link.href.includes('linkedin.com'))[0]?.href || '';
      let phone = '';
      let email = '';
      if (/\d\d\d\s\d\d\d-\d\d-\d\d/.test(card && card.getElementsByClassName('dl-horizontal')[1]?.getElementsByTagName('dd')[1]?.innerText)) {
        phone = card.getElementsByClassName('dl-horizontal')[1]?.getElementsByTagName('dd')[1]?.innerText || '';
      } else {
        phone = card.getElementsByClassName('dl-horizontal')[1]?.getElementsByTagName('dd')[0]?.innerText || '';
      }
      if (/@/.test(card.getElementsByClassName('dl-horizontal')[1]?.getElementsByTagName('dd')[1]?.innerText)) {
        email = card.getElementsByClassName('dl-horizontal')[1]?.getElementsByTagName('dd')[1]?.innerText || '';
      } else {
        email = card.getElementsByClassName('dl-horizontal')[1]?.getElementsByTagName('dd')[2]?.innerText || '';
      }
      response = {
        firstName: card.getElementsByTagName('h1')[0]?.innerText.split(' ')[1] || '',
        lastName: card.getElementsByTagName('h1')[0]?.innerText.split(' ')[0] || '',
        city: card.getElementsByClassName('dl-horizontal')[0]?.getElementsByTagName('dd')[1]?.innerText || '',
        country: 'Україна',
        phone,
        email,
        source: window.location.href || '',
        age: card.getElementsByClassName('dl-horizontal')[0]?.getElementsByTagName('dd')[0]?.innerText?.split(/\s/)[0] || '',
        experience: experience || '',
        linkedin: linkedInLink,
        token: '',
        boardName: 'work.ua',
        fileLink: '.download-resume',
      };
    }

    if (msg.url.includes('https://rabota.ua/')) {
      const contacts = document.getElementsByTagName('alliance-employer-resume-contacts')[0];
      const brief = document.getElementsByTagName('alliance-employer-resume-brief-info')[0];
      const name = document.getElementsByTagName('alliance-shared-ui-copy-to-clipboard')[0]?.getElementsByTagName('p')[0];
      const experienceCard = document.querySelector('alliance-shared-ui-prof-resume-experience');
      const experienceHTML = new DOMParser().parseFromString(experienceCard.innerHTML, 'text/html').body;
      const lists = experienceHTML.querySelectorAll('ul');
      lists && lists.length > 0 && lists.forEach(l => l.remove());
      experienceHTML.querySelector('div') && experienceHTML.querySelector('div').remove();
      const experience = experienceHTML.innerHTML.replace(/<[^\/>]*>/g, '').replace(/<[^>]*>/g, ';').replace(/&nbsp;/g, ' ').replace(/;{2,}/g, '').replace('Досягнення: ', '');
      let firstName = '';
      let lastName = '';
      let email = '';
      if (name.innerText.split(' ').length === 3) {
        firstName = name.innerText.split(' ')[0] || '';
        lastName = name.innerText.split(' ')[2] || '';
      } else {
        firstName = name.innerText.split(' ')[0] || '';
        lastName = name.innerText.split(' ')[1] || '';
      }
      if (/@/.test(contacts.getElementsByTagName('p')[2]?.innerText)) {
        email = contacts.getElementsByTagName('p')[2]?.innerText || '';
      } else {
        email = contacts.getElementsByTagName('p')[4]?.innerText || '';
      }
      response = {
        firstName,
        lastName,
        city: brief.getElementsByTagName('p')[0]?.innerText || '',
        country: 'Україна',
        phone: contacts.getElementsByTagName('p')[0]?.innerText || '',
        email,
        source: window.location.href || '',
        age: Array.prototype.slice.call(document.querySelector('alliance-employer-resume-brief-info').getElementsByTagName('span')).filter(el => el.textContent.match(/років|рік|роки/))[0]?.innerText?.split(' ')[0] || '',
        portfolio: Array.prototype.slice.call(document.getElementsByTagName('h4')).filter(el => el.textContent.match(/Портфолио/))[0]?.nextElementSibling.innerText || '',
        experience,
        token: '',
        boardName: 'rabota.ua',
        fileLink: 'lib-desktop-download-cv-control button',
        linkedin: Array.prototype.slice.call(document.getElementsByTagName('a')).filter(el => el.href.match(/linkedin/))[0]?.href || '',
      }
    }

    if (msg.url.includes('https://ua.jooble.org/')) {
      const card = document.querySelectorAll('div[class^="activeCandidate-module__employee"]')[0];
      const experienceCard = document.querySelector('[data-test-attr="profile-work-experience"]');
      const experienceHTML = experienceCard && new DOMParser().parseFromString(experienceCard.innerHTML, 'text/html').body;
      experienceHTML && experienceHTML.querySelector('h2').remove();
      const lists = experienceHTML && experienceHTML.querySelectorAll('[data-test-attr="profile-work-responsibility"]');
      lists && lists.length > 0 && lists.forEach(l => l.remove());
      const experience = experienceHTML && experienceHTML.innerHTML.replace(/<[^\/>]*>/g, '').replace(/<[^>]*>/g, ';');
      response = {
        firstName: card.querySelector('[data-test-attr="active-user-name"]')?.innerHTML.split(' ')[0] || '',
        lastName: card.querySelector('[data-test-attr="active-user-name"]')?.innerHTML.split(' ')[1] || '',
        city: card.querySelector('[data-test-attr="active-profile-region"]')?.innerHTML || '',
        country: 'Україна',
        phone: card.querySelector('[data-test-attr="active-user-phone"]')?.innerHTML || '',
        email: card.querySelector('[data-test-attr="active-user-email"]')?.innerHTML || '',
        source: window.location.href || '',
        age: card.querySelector('[data-test-attr="active-profile-age"]')?.innerHTML.split(' ')[0] || '',
        experience,
        token: '',
        boardName: 'jooble.org',
        fileLinkAlt: 'button[data-test-btn="download-profile-btn"]',
        fileLink: 'div[data-test-attr="download-cv-candidate"] button',
      }
    }

    if (msg.url.includes('https://dou.ua/')) {
      response = {
        firstName: document.querySelector('.head').querySelector('h1').innerText.split(' ')[0] || '',
        lastName: document.querySelector('.head').querySelector('h1').innerText.split(' ')[1] || '',
        city: document.querySelector('.b-user-info').querySelector('.city').innerHTML.replace(/[^а-яА-Яїі]+/g, '').replace('Місто', '') || '',
        country: 'Україна',
        phone: '',
        email: '',
        source: window.location.href || '',
        age: '',
        token: '',
        boardName: 'dou.ua'
      }
    }

    if (msg.url.includes('https://www.olx.ua/') || msg.url.includes('https://olx.ua/')) {
      response = {
        firstName: document.querySelector('[data-testid="application-title"]')?.innerHTML.split(' ')[0] || '',
        lastName: document.querySelector('[data-testid="application-title"]')?.innerHTML.split(' ')[1] || '',
        city: '',
        country: 'Україна',
        phone: (document.querySelector('[data-testid="application-card-phone-number"]') as HTMLElement).innerText || '',
        email: Array.prototype.slice.call(document.getElementsByTagName('dt')).filter(el => el.textContent.match(/@/))[0]?.innerText || '',
        source: window.location.href || '',
        age: '',
        token: '',
        boardName: 'olx.ua'
      }
    }
    
    if (msg.url.includes('https://djinni.co/')) {
      const name = document.querySelector('#candidate_name');
      response = {
        firstName: name?.innerHTML.split(' ')[0] || '',
        lastName: name?.innerHTML.split(' ')[1] || '',
        city: '',
        country: 'Україна',
        phone: (document.querySelector('.bi-telephone')?.nextElementSibling as HTMLElement)?.innerText || '',
        email: (document.querySelector('#candidate_email') as HTMLElement)?.innerText || '',
        source: Array.prototype.slice.call(document.getElementsByTagName('p')).filter(el => el.innerHTML.match(/Вакансія/))[0]?.querySelector('a').href || '',
        linkedin: (document.querySelector('.bi-linkedin')?.nextElementSibling as HTMLAnchorElement)?.href || '',
        portfolio: Array.prototype.slice.call(document.getElementsByTagName('a')).filter(el => el.innerHTML.match(/CV\/Portfolio/))[0]?.href || '',
        age: '',
        token: '',
        boardName: 'djinni.co',
        directLink: (document.querySelector('a[data-original-title="Резюме профілю"]') as HTMLAnchorElement)?.href || (document.querySelector('a[data-original-title="Відкрите Резюме"]') as HTMLAnchorElement)?.href || '',
      }
    }

    sendResponse(response)
  }

  // Grab token from meta tag from auth window
  if (msg.type === 'GET_TOKEN') {
    const response = {
      token: document.getElementsByTagName('meta')[3].content,
    }

    sendResponse(response)
  }

  // Call click event on file link
  if (msg.type === 'FILE_LINK_CLICK') {
    if (msg.directLink) {
      chrome.runtime.sendMessage({ type: 'INITIATE_DOWNLOAD_FILE', directLink: msg.directLink });
      return;
    }
    if (document.querySelector(msg.link)) {
      return (document.querySelector(msg.link) as HTMLAnchorElement)?.click();
    } else {
      return (document.querySelector(msg.linkAlt) as HTMLAnchorElement)?.click();
    }
  }

  // Handle close plugin event from 
  if (msg.type === 'CLOSE_FRAME') {
    console.log('CLOSE_FRAME');
    requestAnimationFrame(() => {
      setTimeout(() => {
        (document.querySelector('#plugin-iframe') as HTMLElement).style.width = '0px';
        (document.querySelector('.close-frame') as HTMLElement).style.right = '-10px';
      })
    });
    document.querySelector('#plugin-iframe')?.addEventListener('transitionend', () => {
      document.querySelector('.plugin-wrapper').remove();
    });
    chrome.storage.local.set({ popupOpen: false });
  }

  const body = document.body;
  body.style.margin = '0';

  const div = document.createElement('div');
  const iframe = document.createElement('iframe');
  const span = document.createElement('span');

  div.style.height = '100vh';
  div.style.position = 'absolute';
  div.className = 'plugin-wrapper';

  span.style.position = 'fixed';
  span.style.color = '#000';
  span.style.zIndex = '99999';
  span.style.right = '-10px';
  span.style.fontSize = '20px';
  span.style.cursor = 'pointer';
  span.style.top = '0px';
  span.style.transition = '1s';
  span.innerHTML = '&#x2715;';
  span.className = 'close-frame';

  iframe.id = 'plugin-iframe';
  iframe.style.background = "white";
  iframe.style.height = "100%";
  iframe.style.width = "0px";
  iframe.style.position = "fixed";
  iframe.style.top = "0px";
  iframe.style.right = "0px";
  iframe.style.zIndex = "9999";
  iframe.style.border = "0";
  iframe.style.borderLeft = "1px solid #ccc";
  iframe.style.transition = '1s';
  iframe.src = chrome.runtime.getURL("../../index.html");

  div.appendChild(iframe);
  div.appendChild(span);

  document.addEventListener('visibilitychange', () => {
    if (document.visibilityState === 'hidden') {
      chrome.storage.local.get(["popupOpen"], (result) => {
        if (result.popupOpen === true) {
          chrome.storage.local.set({ popupOpen: false });
          chrome.runtime.sendMessage({
            type: "SAVE_STATE",
          });
          document.querySelector('.plugin-wrapper').remove();
        }
      });
    }
  });
  
  span.addEventListener('click', () => {
    chrome.runtime.sendMessage({
      type: "SAVE_STATE",
    });
    requestAnimationFrame(() => {
      setTimeout(() => {
        (document.querySelector('#plugin-iframe') as HTMLElement).style.width = '0px';
        (document.querySelector('.close-frame') as HTMLElement).style.right = '-10px';
      })
    });
    document.querySelector('#plugin-iframe')?.addEventListener('transitionend', () => {
      document.querySelector('.plugin-wrapper').remove();
    });
    chrome.storage.local.set({ popupOpen: false });
  });
  
  if (msg.type === 'toggle') {
    if (msg.popupOpen === true) {
      body.appendChild(div);
      requestAnimationFrame(() => {
        setTimeout(() => {
          span.style.right = '10px';
          iframe.style.width = '420px';
        })
      })
      return;
    }
    if (msg.popupOpen === false) {
      requestAnimationFrame(() => {
        setTimeout(() => {
          (document.querySelector('#plugin-iframe') as HTMLElement).style.width = '0px';
          (document.querySelector('.close-frame') as HTMLElement).style.right = '-10px';
        })
      });
      document.querySelector('#plugin-iframe')?.addEventListener('transitionend', () => {
        document.querySelector('.plugin-wrapper').remove();
      });
      return;
    }
  }

}

/**
 * TODO floating button for opening/closing plugin
 */
// const div2 = document.createElement('div');
// div2.textContent = 'Loading...';
// const image = document.createElement('img');
// image.src = chrome.runtime.getURL('../../job.svg');
// div2.appendChild(image);
// const body = document.body;
// body.appendChild(div2);

/**
 * Fired when a message is sent from either an extension process or a content script.
 */
chrome.runtime.onMessage.addListener(messagesFromReactAppListener);