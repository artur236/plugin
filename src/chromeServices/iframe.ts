const plugin: HTMLElement = document.createElement('div');
const iframe: HTMLIFrameElement = document.createElement('iframe');
const closeIcon: HTMLElement = document.createElement('span');

const body: HTMLElement = document.body;
body.style.margin = '0';

plugin.style.height = '100vh';
plugin.style.position = 'absolute';
plugin.className = 'plugin-wrapper';

closeIcon.style.position = 'fixed';
closeIcon.style.color = '#000';
closeIcon.style.zIndex = '99999';
closeIcon.style.right = '-10px';
closeIcon.style.fontSize = '20px';
closeIcon.style.cursor = 'pointer';
closeIcon.style.top = '0px';
closeIcon.style.transition = '1s';
closeIcon.innerHTML = '&#x2715;';
closeIcon.className = 'close-frame';

iframe.id = 'plugin-iframe';
iframe.style.background = "white";
iframe.style.height = "100%";
iframe.style.width = "0px";
iframe.style.position = "fixed";
iframe.style.top = "0px";
iframe.style.right = "0px";
iframe.style.zIndex = "9999";
iframe.style.border = "0";
iframe.style.borderLeft = "1px solid #ccc";
iframe.style.transition = '1s';
iframe.src = chrome.runtime.getURL("../../index.html");

plugin.appendChild(iframe);
plugin.appendChild(closeIcon);

const closePlugin = (): void => {
  console.log('close');
  chrome.runtime.sendMessage({
    type: "SAVE_STATE",
  });
  requestAnimationFrame((): void => {
    setTimeout((): void => {
      (document.querySelector('#plugin-iframe') as HTMLElement).style.width = '0px';
      (document.querySelector('.close-frame') as HTMLElement).style.right = '-10px';
    })
  });
  document.querySelector('#plugin-iframe')?.addEventListener('transitionend', (): void => {
    document.querySelector('.plugin-wrapper').remove();
  });
  chrome.storage.local.set({ popupOpen: false });
};

const opnePlugin = (): void => {
  console.log('open');
  body.appendChild(plugin);
  requestAnimationFrame(() => {
    setTimeout(() => {
      closeIcon.style.right = '10px';
      iframe.style.width = '420px';
    })
  })
};

closeIcon.addEventListener('click', (): void => {
  closePlugin();
});

export { plugin, closePlugin, opnePlugin };