import { Button, Result, Radio, Space, Popconfirm } from "antd";
import {
  LogoutOutlined,
  LoginOutlined,
  CopyOutlined,
  EditOutlined,
} from "@ant-design/icons";

function Welcome({
  changeEnv,
  parsePage,
  fillForm,
  isParsing,
  signIn,
  signOut,
  token,
}: {
  changeEnv: any;
  parsePage: any;
  fillForm: any;
  isParsing: boolean;
  signIn: any;
  signOut: any;
  token: any;
}) {
  return (
    <div className="App">
      {!token ? (
        <div className="sign-in">
          <Result
            status="403"
            title="Для використання плагіна потрібно авторизуватись"
            subTitle="Оберіть середовище для роботи"
            extra={[
              <>
                <Space direction="vertical" size="small">
                  <Radio.Group
                    onChange={(e) => changeEnv(e.target.value)}
                    defaultValue="stage"
                  >
                    <Radio.Button value="prod">Prod</Radio.Button>
                    <Radio.Button value="stage">Stage</Radio.Button>
                  </Radio.Group>
                  <Button
                    type="primary"
                    onClick={signIn}
                    icon={<LoginOutlined />}
                  >
                    Увійти
                  </Button>
                </Space>
              </>,
            ]}
          />
        </div>
      ) : (
        <div className="welcome-view">
          {!isParsing ? (
            <Result
              status="404"
              title="Шлях Перевершника"
              subTitle="Щоб створити кандидата і перенести інформацію у систему, натисніть на кнопку 'Сканувати', або 'Додати вручну'"
              extra={[
                <>
                  <Space direction="vertical" size="middle">
                    <Button
                      type="primary"
                      onClick={parsePage}
                      loading={isParsing && true}
                      icon={<CopyOutlined />}
                    >
                      Сканувати сторінку
                    </Button>
                    <Button
                      type="default"
                      onClick={fillForm}
                      loading={isParsing && true}
                      icon={<EditOutlined />}
                    >
                      Додати вручну
                    </Button>
                    {token ? (
                      <Popconfirm
                        placement="bottom"
                        title="Вихід з облікового запису"
                        description="Ви впевнені що хочете вийти зі свого облікового запису?"
                        onConfirm={signOut}
                        okText="Так"
                        cancelText="Скасувати"
                      >
                        <Button style={{ marginTop: '10px' }} type="default" danger icon={<LogoutOutlined />}>
                          Вийти
                        </Button>
                      </Popconfirm>
                    ) : null}
                  </Space>
                </>,
              ]}
            />
          ) : (
            <Result
              status="info"
              title="Виконується сканування даних. Зачекайте"
            />
          )}
        </div>
      )}
    </div>
  );
}

export default Welcome;
