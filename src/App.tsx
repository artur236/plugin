import React from "react";
import axios from "axios";
import { Result, Spin } from "antd";
import "./App.css";
import Welcome from "./Welcome";
// import Loader from './Loader';
import Form from "./Form";
import { DOMMessage, DOMMessageResponse } from "./types";

function App() {
  const [data, setData] = React.useState<DOMMessageResponse | {}>({});

  const [isWelcomeHidden, setIsWelcomeHidden] = React.useState<boolean>(false);
  const [isParsing, setIsParsing] = React.useState<boolean>(false);
  const [isLogin, setIsLogin] = React.useState<boolean>(false);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [url, setUrl] = React.useState<string>("");
  const [token, setToken] = React.useState<any>("");
  const [env, setEnv] = React.useState<any>("");
  const [stream, setStream] = React.useState<any>(
    "https://jdc.hcm.test.silpo.ua"
  );
  const [userID, setUserID] = React.useState<any>("");
  const [boardsList, setBoardsList] = React.useState<any>([]);
  const [jobsList, setJobsList] = React.useState<any>([]);
  const [businessList, setBusinessList] = React.useState<any>([]);

  // Connect to background script
  chrome.runtime.connect({ name: "popup" });

  function envString(env: string) {
    if (env === "prod") {
      return setStream("https://jdc.hcm.silpo.ua");
    }
    return setStream("https://jdc.hcm.test.silpo.ua");
  }

  async function fetchBoardsList() {
    const boardsList = await axios({
      url: `${stream}/api/v2/addons/GetBoardsList`,
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const { data: boards } = boardsList;
    setBoardsList(boards.data);
  }

  async function fetchActualJobsList() {
    const jobsList = await axios({
      url: `${stream}/api/v2/addons/getActualJobsList`,
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const { data: jobs } = jobsList;
    setJobsList(jobs.data);
    setBusinessList([
      ...new Set(
        jobs.data
          .map((t: { business: any }) => t.business)
          .filter((f: any) => f)
      ),
    ]);
  }

  React.useEffect(() => {
    /**
     * TODO
     * 1. Update data on form's input changes
     * 2. Clear data on form's submit +
     */
    chrome.storage.local.get(
      ["isWelcomeHidden", "data", "url"],
      ({ isWelcomeHidden, data, url }) => {
        setIsWelcomeHidden(isWelcomeHidden);
        setData(data);
        if (url) {
          setUrl(url);
        }
      }
    );
  }, [isWelcomeHidden, data, url]);

  React.useEffect(() => {
    chrome.storage.local.get(["token", "env"], ({ token, env }) => {
      setToken(token);
      setEnv(env);
      envString(env);
    });
    // If token exists, check if it's valid
    if (token && token.length > 0) {
      axios({
        url: `${stream}/api/v2/addons/CheckToken`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((resp) => {
          const csod_id = resp.data.data.csod_id;
          setUserID(csod_id);
          fetchBoardsList();
          fetchActualJobsList();
        })
        .catch((err) => {
          signOut();
        });
    }
    // If user press Parse page button
    if (isParsing) {
      // Grab active tab
      chrome.tabs &&
        chrome.tabs.query(
          {
            active: true,
            currentWindow: true,
          },
          (tabs) => {
            // Send message to content script to grab DOM
            chrome.tabs.sendMessage(
              tabs[0].id || 0,
              { type: "GET_DOM", url: tabs[0].url } as DOMMessage,
              (parsedData: DOMMessageResponse) => {
                setIsWelcomeHidden(true);
                setUrl(tabs[0].url);
                setData(parsedData);
                // Save data to local storage
                chrome.storage.local.set({ data: parsedData });
                chrome.storage.local.set({ isWelcomeHidden: true });
                chrome.storage.local.set({ url: tabs[0].url });
              }
            );
          }
        );
    }
    // If no token, open login page
    if (isLogin && !token) {
      // Create new window
      chrome.windows.create(
        {
          url: "index.html",
          type: "popup",
          width: 1000,
          height: 1000,
        },
        (wind) => {
          // Grab active tab
          chrome.tabs &&
            chrome.tabs.query(
              {
                active: true,
                currentWindow: true,
              },
              (tabs) => {
                // Update tab url
                chrome.tabs.update(
                  wind.tabs[0].id,
                  { url: `${stream}/api/v2/addons/qauth/start` },
                  (tab) => {
                    // Listen for tab url change
                    chrome.tabs.onUpdated.addListener((id, info, tab) => {
                      // If url contains success, grab token
                      if (
                        info.url &&
                        info.url.includes(
                          `${stream}/api/v2/addons/qauth/success`
                        )
                      ) {
                        // Send message to content script to grab token
                        setTimeout(() => {
                          chrome.tabs.sendMessage(
                            wind.tabs[0].id || 0,
                            { type: "GET_TOKEN" },
                            (response) => {
                              setToken(response && response.token);
                              chrome.storage.local.set(
                                { token: response.token },
                                () => {
                                  setIsLogin(false);
                                  setIsLoading(false);
                                  // Close window
                                  chrome.tabs.remove(wind.tabs[0].id);
                                }
                              );
                            }
                          );
                        }, 3000);
                      }
                    });
                  }
                );
              }
            );
        }
      );
    }
  }, [isParsing, isLogin, token]);

  const parsePage = () => {
    setIsParsing(true);
  };

  const fillForm = () => {
    // Get active tab url and write it into state
    chrome.tabs &&
      chrome.tabs.query(
        {
          active: true,
          currentWindow: true,
        },
        (tabs) => {
          setUrl(tabs[0].url);
          chrome.storage.local.set({ url: tabs[0].url });
        }
      );
    chrome.storage.local.set({ isWelcomeHidden: true });
    chrome.storage.local.set({ data: null });
    setIsWelcomeHidden(true);
    setData({});
  };

  const closeForm = () => {
    chrome.storage.local.set({ isWelcomeHidden: false });
    chrome.storage.local.set({ data: null });
    chrome.storage.local.set({ url: null });
    setIsWelcomeHidden(false);
    setIsParsing(false);
  };

  const signIn = () => {
    setIsLogin(true);
    setIsLoading(true);
  };

  const signOut = () => {
    chrome.storage.local.clear(() => {
      setToken("");
      setEnv("");
    });
  };

  const changeEnv = (env: string) => {
    chrome.storage.local.set({ env: env }, () => {
      setEnv(env);
      envString(env);
    });
  };

  // const closeFrame = () => {
    // chrome.runtime.sendMessage({
    //   type: "SAVE_STATE",
    // });
    // chrome.tabs &&
    //     chrome.tabs.query(
    //       {
    //         active: true,
    //         currentWindow: true,
    //       },
    //       (tabs) => {
    //         // Send message to content script to close frame
    //         chrome.tabs.sendMessage(
    //           tabs[0].id || 0,
    //           { type: "CLOSE_FRAME", url: tabs[0].url } as DOMMessage,
    //         );
    //       }
    //     );
  // };

  return (
    <>
      {/* <span onClick={() => closeFrame()} className="close-frame">&#x2715;</span> */}
      {isLoading ? (
        <div className="welcome-view">
          <Result
            title="Відбувається авторизація..."
            subTitle="Слідуйте вказівкам у спливаючому вікні"
            extra={<Spin size="large" />}
          />
        </div>
      ) : (
        <div className="content">
          {!isWelcomeHidden ? (
            <Welcome
              changeEnv={changeEnv}
              parsePage={parsePage}
              fillForm={fillForm}
              isParsing={isParsing}
              signIn={signIn}
              signOut={signOut}
              token={token}
            />
          ) : null}
          {isWelcomeHidden ? (
            <Form
              stream={stream}
              closeForm={closeForm}
              parsedData={data}
              businessList={businessList}
              boardsList={boardsList}
              jobsList={jobsList}
              token={token}
              userID={userID}
              url={url}
            />
          ) : null}
        </div>
      )}
    </>
  );
}

export default App;
