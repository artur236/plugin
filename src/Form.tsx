// @ts-nocheck
import React from "react";
import { useForm, Controller } from "react-hook-form";
import axios from "axios";
import Select from "react-select";
import Highlighter from "react-highlight-words";
import {
  UploadOutlined,
  DeleteOutlined,
  FilterOutlined,
  FilterFilled,
  ArrowLeftOutlined,
} from "@ant-design/icons";
import {
  Button,
  Result,
  Checkbox,
  Radio,
  Input,
  Typography,
  Space,
  Layout,
  Col,
  Row,
  Divider,
  Card,
} from "antd";
import { DOMMessage, DOMMessageResponse } from "./types";

type FormValues = {
  firstName: string;
  lastName: string;
  middleName: string;
  email: string;
  phone: string;
  city: string;
  country: string;
  source: string;
  age: string;
  experience: string;
  vacancy: string;
  adds?: string;
  server: string;
  myVacancies: boolean;
  resume: string;
  file: any;
  portfolio: string;
  linkedin: string;
  comment: string;
};

/**
 *
 * @param closeForm Function that returns to main screen
 * @returns
 */
function Done({
  closeForm,
  candidateId,
}: {
  closeForm: () => void;
  candidateId: string;
}) {
  return (
    <Row align='middle' style={{ height: '100vh' }}>
      <Col span={24}>
      <Result
        status="success"
        title="Кандидата додано!"
        subTitle={`ID Кандидата: ${candidateId}`}
        extra={[
          <>
            <Button type="primary" key="main" onClick={closeForm}>
              На головну
            </Button>
          </>,
        ]}
      />
      </Col>
    </Row>
  );
}
/**
 * parseNumber
 * @param number
 * @returns Phone number as string without spaces and other symbols
 */
const parseNumber = (number: string) => {
  const trimmedNumber = number.match(/\d/g).join("");
  return trimmedNumber;
};
/**
 * Form component
 */
export default function FormPage({
  stream,
  closeForm,
  parsedData,
  businessList = [],
  boardsList = [],
  jobsList = [],
  token,
  userID,
  url,
}: {
  stream: string;
  closeForm: () => void;
  parsedData: DOMMessageResponse;
  businessList: [];
  boardsList: [];
  jobsList: [];
  token: string;
  userID: string;
  url: string;
}) {
  /**
   * Listen for background script
   * When donwload window appear on screen
   * background script send message with file
   * that was downloaded
   */
  chrome.runtime.onMessage.addListener((msg) => {
    if (msg.type === "file") {
      if (parsedData) {
        parsedData.file = msg.item.url;
      }
      let fileForInput = new File(
        [msg.item],
        msg.item.filename,
        { type: msg.item.mime, lastModified: new Date().getTime() },
        "utf-8"
      );
      let container = new DataTransfer();
      container.items.add(fileForInput);
      document.querySelector("#file-input").files = container.files;
      setLocalFile(undefined);
      setFile(document.querySelector("#file-input")?.files[0]);
      setFileUploaded(true);
      setLoading(false);
      setIsLoading(false);
      clearErrors("file");
      /**
       * TODO if contacts closed hide loading
       */
    }
    if (msg.type === 'SAVE_STATE') {
      const formFields = getValues();
      delete formFields.resume;
      delete formFields.file;
      if (Object.hasOwn(formFields, 'vacancy')) {
        formFields.vacancy = vacancy;
      }
      console.log(formFields);
      chrome.storage.local.set({ data: formFields });
    }
  });

  /**
   * Form state
   */
  const [isDone, setIsDone] = React.useState<boolean>(false);
  // Loading state for save candidate process
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  // State for My vacancies only filter
  const [myVacancies, setMyVacancies] = React.useState<boolean>(true);
  const [vacancy, setVacancy] = React.useState<any>(null);
  // Loading state for file uploader
  const [loading, setLoading] = React.useState<boolean>(undefined);
  // State for filter dropdown
  const [filterOpen, setFilterOpen] = React.useState<boolean>(false);
  // State for businessess list in filter
  const [businessFilter, setBusinessFilter] = React.useState<any[]>([]);
  // Write local file to state
  const [localFile, setLocalFile] = React.useState<any>();
  // State for file uploaded
  const [fileUploaded, setFileUploaded] = React.useState<boolean>(false);
  // Candidate ID
  const [candidateId, setCandidateId] = React.useState<string>("");

  const [file, setFile] = React.useState<any>(null);

  const fileInput = document.querySelector("#file-input");

  let jobOptions = jobsList.map((j: any, i: any) => {
    return {
      ...j,
      id: i,
      value: j.req,
      label: `(${j.req}) - ${j.title.value} - ${j.location?.adds}`,
      search: `${j.req} ${j.title.value} ${j.location?.city} ${j.location?.adds}`,
    };
  });

  if (businessFilter && businessFilter.length > 0) {
    jobOptions = jobOptions.filter((job) =>
      businessFilter.some((business) => business === job.business)
    );
  }

  if (myVacancies) {
    jobOptions = jobOptions.filter((job) => job.owner_id === userID);
  }

  /**
   * Listen for dropdown open/close
   */
  const handleOpenDropdownFilter = () => {
    setFilterOpen(!filterOpen);
  };

  /**
   * Handle business filter change
   */
  const businessFilterChange = (business) => {
    const index = businessFilter.findIndex((b) => b === business);
    if (index !== -1) {
      setBusinessFilter([
        ...businessFilter.slice(0, index),
        ...businessFilter.slice(index + 1),
      ]);
      return;
    }
    setBusinessFilter([...businessFilter, business]);
  };

  /**
   * Form handler
   */
  const {
    register,
    handleSubmit,
    control,
    reset,
    setError,
    clearErrors,
    formState: { errors },
    setValue,
    getValues,
  } = useForm<FormValues>({
    reValidateMode: "onBlur" | "onChange",
  });

  /**
   * Based on current url grab needed job board link
   */
  const src: { board_id: string; board_name: string; board_url: string } =
    boardsList.filter(
      (b: { board_id: string; board_name: string; board_url: string }) =>
        url.includes(b.board_url.replace("https://", ""))
    )[0];

  /**
   * Handle resume checkbox change
   * Accept `checked` property of checkbox input
   * If config provides `fileLink` for website
   * Send message to content script to click on that link
   * @param target
   */
  const handleResumeChange = (target: boolean) => {
    if (target) {
      if (parsedData.fileLink || parsedData.directLink) {
        setLoading(true);
        setIsLoading(true);
        chrome.tabs &&
          chrome.tabs.query(
            {
              active: true,
              currentWindow: true,
            },
            (tabs) => {
              chrome.tabs.sendMessage(tabs[0].id || 0, {
                type: "FILE_LINK_CLICK",
                url: tabs[0].url,
                link: parsedData.fileLink,
                linkAlt: parsedData.fileLinkAlt || undefined,
                directLink: parsedData.directLink || undefined,
              } as DOMMessage);
            }
          );
      }
    } else {
      parsedData.file = "";
      document.querySelector("#file-input").value = null;
      setFileUploaded(false);
      setFile(null);
    }
  };

  const handleFileInputChange = (target: any) => {
    parsedData.file = undefined;
    if (target.files && target.files.length > 0) {
      setLocalFile(target.files[0]);
      setFile(target.files[0]);
      setFileUploaded(true);
      setValue("resume", true);
      clearErrors(["server" ,"file"]);
      return;
    } else {
      setLocalFile(undefined);
      setFile(null);
      setFileUploaded(false);
      setValue("resume", false);
      return;
    }
  };

  const clearFileInput = () => {
    setFileUploaded(false);
    setFile(null);
    fileInput.value = null;
    setValue("resume", false);
  };

  const customFilterOption = (option, rawInput) => {
    const words = rawInput.split(" ");
    return words.reduce(
      (acc, cur) =>
        acc && option.data.search.toLowerCase().includes(cur.toLowerCase()),
      true
    );
  };

  const optionRenderer = ({ label }, { inputValue }) => {
    const words = inputValue.split(" ");
    return <Highlighter searchWords={words} textToHighlight={label} />;
  };

  /**
   * Form submit
   */
  const onSubmit = handleSubmit(async (data) => {
    let fileId;
    /**
     * Save Candidate to CSOD
     * @param fileId If resume checkbox was checked, `fileId` parameter needed
     */
    const saveCandidate = async (fileId?: any) => {
      const JobId = data.vacancy.req ? data.vacancy.req : data.vacancy;
      await axios({
        url: `${stream}/api/v2/addons/SaveCandidate`,
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          JobId,
          boardName: src ? src.board_name : parsedData.boardName,
          boardUrl: url,
          lang: "uk-UA",
          candidate: {
            email: data.email,
            firstName: data.firstName,
            lastName: data.lastName,
            age: data.age,
            contactDetails: {
              PhoneNumber: data.phone ? parseNumber(data.phone) : "0000",
              city: data.city,
              country: data.country,
            },
          },
          resume: {
            fileId,
            skills: data.experience,
          },
          links: {
            portfolio: data.portfolio,
            linkedin: data.linkedin,
          },
          comment: data.comment,
        },
      })
        .then((res) => {
          setIsDone(true);
          setIsLoading(false);
          setCandidateId(res.data.data.CandidateId);
        })
        .catch((err) => {
          setIsLoading(false);
          let errors;
          if (err.response.data && err.response.data.Errors) {
            errors = Object.values(err.response.data.Errors).join("\r\n");
            setError("server", {
              type: "server",
              message:
                `${err.response.data.Message}  <br />${errors}` ||
                "Виникла помилка на сервері",
            });
            return;
          } else {
            setError("server", {
              type: "server",
              message: "Виникла помилка на сервері",
            });
          }
        });
    };
    setIsLoading(true);
    // In case download resume checkbox was checked
    // Download needed file
    // When file is ready upload it to CSOD
    if (data.resume) {
      if (!fileUploaded) {
        setIsLoading(false);
        return setError("file", { type: "file", message: "Виберіть файл" });
      }
      if (localFile) {
        const formData = new FormData();
        const filename = localFile.name;
        const fileInput = document.querySelector("#file-input");
        formData.append("file", fileInput.files[0]);
        formData.append("name", filename);
        axios
          .post(
            `${stream}/api/v2/addons/AddFile?JobId=${data.vacancy}&type=resume`,
            formData,
            {
              headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": fileInput.files[0].type,
              },
            }
          )
          .then((response) => {
            fileId = response.data.data.id;
            return saveCandidate(fileId);
          })
          .catch((err) => {
            setIsLoading(false);
            let errors;
            if (err.response.data && err.response.data.Errors) {
              errors = Object.values(err.response.data.Errors).join("\r\n");
              setError("server", {
                type: "server",
                message:
                  `${err.response.data.Message}  <br />${errors}` ||
                  "Виникла помилка на сервері",
              });
              return;
            } else {
              setError("server", {
                type: "server",
                message: "Виникла помилка на сервері",
              });
            }
          });
      }
      if (parsedData.file && parsedData.file.length > 0) {
        axios
          .get(parsedData.file, {
            responseType: "blob",
          })
          .then((res) => {
            let type;
            switch (res.data.type) {
              case "application/pdf":
                type = "pdf";
                break;
              case "application/octet-stream":
                type = "docx";
                break;
              case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                type = "docx";
                break;
            }
            const formData = new FormData();
            const filename = `${Math.floor(
              Math.random() * (200 - 100) + 100
            )}-${Math.floor(Math.random() * (2000 - 1000) + 1000)}.${type}`;
            const file = new Blob([res.data], { type });
            formData.append("file", file, filename);
            formData.append("name", filename);
            axios
              .post(
                `${stream}/api/v2/addons/AddFile?JobId=${data.vacancy}&type=resume`,
                formData,
                {
                  headers: {
                    Authorization: `Bearer ${token}`,
                    "Content-Type": res.data.type,
                  },
                }
              )
              .then((r) => {
                fileId = r.data.data.id;
                return saveCandidate(fileId);
              })
              .catch(() => {
                setIsLoading(false);
                setError("server", {
                  type: "server",
                  message: "Виникла помилка на сервері",
                });
              });
          });
      }
      return;
    }
    saveCandidate();
  });

  /**
   * Monitor for parsed data
   * If parsed data present - fill in the form with provided data
   */
  React.useEffect(() => {
    if (parsedData) {
      if (parsedData.vacancy) {
        setValue("vacancy", parsedData.vacancy.req);
        setVacancy(parsedData.vacancy);
      }
      reset(parsedData);
    } else {
      reset();
    }
  }, [
    parsedData,
    myVacancies,
    reset,
    fileInput,
    setValue,
  ]);

  return (
    <>
      {!isDone ? (
        <Layout.Content>
          <Row>
            <Col span={24}>
              <form onSubmit={onSubmit}>
                <Row>
                  <Col span={6}>
                    <Button onClick={() => {closeForm(); reset()}} icon={<ArrowLeftOutlined />}>
                      Назад
                    </Button>
                  </Col>
                  <Col offset={12} span={6}>
                    <Button
                      onClick={() => handleOpenDropdownFilter()}
                      icon={!filterOpen ? <FilterOutlined /> : <FilterFilled />}
                    >
                      Фільтр
                    </Button>
                  </Col>
                </Row>
                <Row className={{ "filter-dropdown": true, open: filterOpen }}>
                  <Col span={24}>
                    <div className="dropdown">
                      <Row>
                        {businessList && businessList.length > 0 ? (
                          <Col span={11}>
                            <Card title="Бізнес">
                              {businessList.map((business) => {
                                return (
                                  <Checkbox
                                    style={{ marginInlineStart: 0 }}
                                    onChange={(e) =>
                                      businessFilterChange(e.target.value)
                                    }
                                    value={business}
                                    checked={businessFilter.includes(business)}
                                  >
                                    {business}
                                  </Checkbox>
                                );
                              })}
                            </Card>
                          </Col>
                        ) : null}
                        <Col span={12} offset={1}>
                          <Card title="Відображення">
                            <Radio
                              checked={myVacancies}
                              onChange={() => setMyVacancies(true)}
                            >
                              Мої вакансії
                            </Radio>
                            <Radio
                              checked={!myVacancies}
                              onChange={() => setMyVacancies(false)}
                            >
                              Усі вакансії
                            </Radio>
                          </Card>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    <Divider />
                  </Col>
                </Row>
                <Row style={{ marginBottom: "10px" }}>
                  <Col span={24}>
                    <Space direction="vertical" size="small">
                      <Typography.Text strong>Вакансія*</Typography.Text>
                      <Controller
                        {...register("vacancy")}
                        control={control}
                        rules={{
                          required: true,
                        }}
                        render={({
                          field: { onChange, onBlur, value, name, ref },
                        }) => (
                          <Select
                            options={jobOptions}
                            onChange={(val) => {
                              onChange(val.value);
                              setVacancy(val);
                            }}
                            filterOption={customFilterOption}
                            formatOptionLabel={optionRenderer}
                            onBlur={() => clearErrors(["server", "vacancy"])}
                            name={name}
                            value={vacancy}
                            ref={ref}
                            placeholder="Оберіть вакансію"
                            noOptionsMessage={() => "Вакансій не знайдено"}
                          />
                        )}
                      />
                      {errors.vacancy && errors.vacancy.type === "required" && (
                        <span className="error">Обов'язково</span>
                      )}
                    </Space>
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    <Space
                      direction="vertical"
                      size="small"
                      className="spaceOverride"
                    >
                      <Typography.Text strong>Ім'я*</Typography.Text>
                      <Controller
                        name="firstName"
                        id="firstName"
                        control={control}
                        rules={{
                          required: true,
                        }}
                        render={({ field }) => <Input status={errors.firstName ? 'error' : ''} onInput={() => clearErrors(['firstName', 'server'])} {...field} />}
                      />
                      {errors.firstName &&
                        errors.firstName.type === "required" && (
                          <span className="error">Обов'язково</span>
                        )}

                      <Typography.Text strong>Прізвище*</Typography.Text>
                      <Controller
                        name="lastName"
                        id="lastName"
                        control={control}
                        rules={{
                          required: true,
                        }}
                        render={({ field }) => <Input status={errors.lastName ? 'error' : ''} onInput={() => clearErrors(['lastName', 'server'])} {...field} />}
                      />

                      {errors.lastName &&
                        errors.lastName.type === "required" && (
                          <span className="error">Обов'язково</span>
                        )}

                      <Typography.Text strong>По батькові</Typography.Text>
                      <Controller
                        name="middleName"
                        id="middleName"
                        control={control}
                        render={({ field }) => <Input {...field} />}
                      />

                      <Typography.Text strong>Email*</Typography.Text>
                      <Controller
                        name="email"
                        id="email"
                        control={control}
                        rules={{
                          required: true,
                        }}
                        render={({ field }) => <Input status={errors.email ? 'error' : ''} onInput={() => clearErrors(['email', 'server'])} {...field} />}
                      />

                      {errors.email && errors.email.type === "required" && (
                        <span className="error">Обов'язково</span>
                      )}

                      <Typography.Text strong>Телефон</Typography.Text>
                      <Controller
                        name="phone"
                        id="phone"
                        control={control}
                        render={({ field }) => <Input {...field} />}
                      />

                      <Typography.Text strong>Вік</Typography.Text>
                      <Controller
                        name="age"
                        id="age"
                        control={control}
                        render={({ field }) => <Input type="number" {...field} />}
                      />

                      <Typography.Text strong>Досвід роботи</Typography.Text>
                      <Controller
                        name="experience"
                        id="experience"
                        control={control}
                        render={({ field }) => <Input {...field} />}
                      />

                      <Typography.Text strong>
                        Посилання на портфоліо
                      </Typography.Text>
                      <Controller
                        name="portfolio"
                        id="portfolio"
                        control={control}
                        render={({ field }) => <Input {...field} />}
                      />

                      <Typography.Text strong>
                        Посилання на LinkedIn
                      </Typography.Text>
                      <Controller
                        name="linkedin"
                        id="linkedin"
                        control={control}
                        render={({ field }) => <Input {...field} />}
                      />

                      <Typography.Text strong>Коментар</Typography.Text>
                      <Controller
                        name="comment"
                        id="comment"
                        control={control}
                        render={({ field }) => <Input {...field} />}
                      />

                      <Typography.Text strong>Місто</Typography.Text>
                      <Controller
                        name="city"
                        id="city"
                        control={control}
                        render={({ field }) => <Input {...field} />}
                      />

                      <Typography.Text strong>Країна</Typography.Text>
                      <Controller
                        name="country"
                        id="country"
                        control={control}
                        render={({ field }) => <Input {...field} />}
                      />

                      <Controller
                        name="resume"
                        id="resume"
                        control={control}
                        render={({ field: { onChange, name, ref, value } }) => (
                          <Checkbox
                            name={name}
                            value={value}
                            ref={ref}
                            checked={value}
                            onChange={(e) => {
                              onChange(e.target.checked);
                              handleResumeChange(e.target.checked);
                            }}
                          >
                            Завантажити резюме
                          </Checkbox>
                        )}
                      />
                    </Space>
                  </Col>
                </Row>
                <Row style={{ marginTop: "10px" }}>
                  <Col span={24}>
                    <div className="file-input-wrapper">
                      {!file ? (
                        <Button
                          onClick={() =>
                            document.querySelector("#file-input").click()
                          }
                          icon={<UploadOutlined />}
                        >
                          Прикріпити файл
                        </Button>
                      ) : (
                        <Space direction="vertical" size="small">
                          <Row>
                            <Col span={16}>
                              <Typography.Text>{file.name}</Typography.Text>
                            </Col>
                            <Col span={2} offset={6}>
                              <Button
                                danger
                                onClick={() => clearFileInput()}
                                icon={<DeleteOutlined />}
                              ></Button>
                            </Col>
                            <Col span={24}>
                              <Button
                                type="dashed"
                                onClick={() =>
                                  document.querySelector("#file-input").click()
                                }
                                icon={<UploadOutlined />}
                              >
                                Обрати інший файл
                              </Button>
                            </Col>
                          </Row>
                        </Space>
                      )}
                      <input
                        {...register("file", {
                          required: getValues("resume") ? true : false,
                        })}
                        style={{ display: "none" }}
                        type="file"
                        id="file-input"
                        onChange={(e: any) => {
                          handleFileInputChange(e.target);
                        }}
                      />
                      {errors.file && (
                        <span className="error">Обов'язково</span>
                      )}
                    </div>
                  </Col>
                </Row>
                <Row style={{ marginTop: "20px" }}>
                  <Col span={24}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="btn-primary"
                      loading={isLoading}
                    >
                      Додати кандидата
                    </Button>
                    {errors.server && errors.server.type === "server" && (
                      <span
                        className="error"
                        dangerouslySetInnerHTML={{
                          __html: errors.server.message,
                        }}
                      ></span>
                    )}
                  </Col>
                </Row>
              </form>
            </Col>
          </Row>
        </Layout.Content>
      ) : (
        <Done closeForm={closeForm} candidateId={candidateId} />
      )}
    </>
  );
}
