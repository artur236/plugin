export type DOMMessage = {
  type: string,
  url?: string,
  link?: any,
  linkAlt?: any,
  directLink?: any,
  popupOpen: boolean,
}

export type DOMMessageResponse = {
  firstName?: string;
  lastName?: string;
  middleName?: string;
  email?: string;
  phone?: string;
  city?: string;
  country?: string;
  source?: string;
  experience?: string;
  age?: string;
  linkedin?: string;
  portfolio?: string;
  vacancy?: string;
  boardName?: string;
  token?: string;
  fileLink?: any;
  fileLinkAlt?: any;
  file?: any;
  directLink?: any;
}

export type Token = {
  token: string;
}
