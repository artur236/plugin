import Enzyme, { shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import App from '../App';

Enzyme.configure({ adapter: new Adapter() });

describe('App', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toMatchSnapshot();
  });
  it('should send a POST request to the server if token not empty', () => {
    const wrapper = shallow(<App />);
    wrapper.setState({ token: '1234' });
    wrapper.find('input[name="email"]').simulate('change', { target: { value: '' } })
  });
});