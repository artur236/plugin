// test Form component
import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Form from '../Form';

Enzyme.configure({ adapter: new Adapter() });

describe('Form', () => {
  it('renders correctly', () => {
    const props = {
      changeEnv: jest.fn(),
      parsePage: jest.fn(),
      fillForm: jest.fn(),
      isParsing: false,
      signIn: jest.fn(),
      signOut: jest.fn(),
      token: null
    }
    const wrapper = shallow(<Form {...props} />);
    expect(wrapper).toMatchSnapshot();
  });
})