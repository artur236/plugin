import Enzyme, { shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import DOMEvaluator from '../DOMEvaluator';

Enzyme.configure({ adapter: new Adapter() });

test('messagesFromReactAppListener should return correct response if msg type is "GET_DOM"', () => {
  const wrapper = shallow(<DOMEvaluator />);
  const instance = wrapper.instance();
  const response = instance.messagesFromReactAppListener({ type: 'GET_DOM' });
  expect(response).toEqual({ type: 'DOM', dom: document.documentElement.outerHTML });
});