// 1. Test to ensure that the Welcome component renders correctly:

import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Welcome from '../Welcome';

Enzyme.configure({ adapter: new Adapter() });

describe('Welcome component', () => {
  it('should render correctly', () => {
    const props = {
      changeEnv: jest.fn(),
      parsePage: jest.fn(),
      fillForm: jest.fn(),
      isParsing: false,
      signIn: jest.fn(),
      signOut: jest.fn(),
      token: null
    };
    const wrapper = shallow(<Welcome {...props} />);
    expect(wrapper).toMatchSnapshot();
  });
});

// 2. Test to ensure that the changeEnv function is called when the radio buttons are clicked:

describe('Welcome: changeEnv', () => {
  it('should call changeEnv when radio buttons are clicked', () => {
    const props = {
      changeEnv: jest.fn(),
      parsePage: jest.fn(),
      fillForm: jest.fn(),
      isParsing: false,
      signIn: jest.fn(),
      signOut: jest.fn(),
      token: null
    };
    const wrapper = shallow(<Welcome {...props} />);
    wrapper.find('input[name="env"]').at(0).simulate('change');
    expect(props.changeEnv).toHaveBeenCalledWith('prod');
    wrapper.find('input[name="env"]').at(1).simulate('change');
    expect(props.changeEnv).toHaveBeenCalledWith('stage');
  });
});

// 3. Test to ensure that the signIn function is called when the sign in button is clicked:

describe('Welcome: signIn', () => {
  it('should call signIn when sign in button is clicked', () => {
    const props = {
      changeEnv: jest.fn(),
      parsePage: jest.fn(),
      fillForm: jest.fn(),
      isParsing: false,
      signIn: jest.fn(),
      signOut: jest.fn(),
      token: null
    };
    const wrapper = shallow(<Welcome {...props} />);
    wrapper.find('.btn-primary').simulate('click');
    expect(props.signIn).toHaveBeenCalled();
  });
});

// 4. Test to ensure that the parsePage and fillForm functions are called when the corresponding buttons are clicked:

describe('Welcome: parsePage', () => {
  it('should call parsePage and fillForm when corresponding buttons are clicked', () => {
    const props = {
      changeEnv: jest.fn(),
      parsePage: jest.fn(),
      fillForm: jest.fn(),
      isParsing: false,
      signIn: jest.fn(),
      signOut: jest.fn(),
      token: 'some-token'
    };
    const wrapper = shallow(<Welcome {...props} />);
    wrapper.find('.btn-primary').at(0).simulate('click');
    expect(props.parsePage).toHaveBeenCalled();
    wrapper.find('.btn-greyed').simulate('click');
    expect(props.fillForm).toHaveBeenCalled();
  });
});