/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */

// Confirm that the plugin has been installed
chrome.runtime.onInstalled.addListener(() => {
  console.log("on install");
});

// Handle plugin icon click
chrome.action.onClicked.addListener((tab) => {
  // Check if popup is open
  chrome.storage.local.get(["popupOpen"], (result) => {
    if (result.popupOpen === null || !result.popupOpen) {
      chrome.storage.local.set({ popupOpen: true });
      chrome.tabs.sendMessage(tab.id, { type: "toggle", popupOpen: true });
    }
    if (result.popupOpen === true) {
      chrome.storage.local.set({ popupOpen: false });
      chrome.tabs.sendMessage(tab.id, { type: "toggle", popupOpen: false });
    }
  });
});

// Connect to popup for checking if popup is open
chrome.runtime.onConnect.addListener(function(port) {
  if (port.name === "popup") {
    popupOpen = true;
      port.onDisconnect.addListener(function() {
        popupOpen = false;
        chrome.storage.local.set({ popupOpen: false });
      });
  }
});

// If parsing website have direct link to file initiate to download it
chrome.runtime.onMessage.addListener((request) => {
  if (request.type === "INITIATE_DOWNLOAD_FILE") {
    chrome.downloads.download({
      url: request.directLink,
    });
  }
});

chrome.downloads.onDeterminingFilename.addListener((item) => {
  // If popup is open, interrupt original download and send message to popup with file item
  if (popupOpen) {
    chrome.runtime.sendMessage({
      type: "file",
      item,
    });
    chrome.downloads.cancel(item.id);
  }
});

// Listen for history state changes to keep background script alive
chrome.webNavigation.onHistoryStateUpdated.addListener((details) => {
  console.log("details", details);
});
